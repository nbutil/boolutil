package boolUtil.fromNumber


/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Turns the given [Byte] in to a [BooleanArray] corresponding to the value of its bits.
 *
 * @return [BooleanArray]
 */
fun Byte.toBooleanArray(): BooleanArray = booleanArrayOf(
	this.getBit(0), this.getBit(1), this.getBit(2), this.getBit(3), 
	this.getBit(4), this.getBit(5), this.getBit(6), this.getBit(7)
)

/**
 * Gets the 1st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Byte.component1(): Boolean = this.getBit(0)

/**
 * Gets the 2nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Byte.component2(): Boolean = this.getBit(1)

/**
 * Gets the 3rd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Byte.component3(): Boolean = this.getBit(2)

/**
 * Gets the 4th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Byte.component4(): Boolean = this.getBit(3)

/**
 * Gets the 5th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Byte.component5(): Boolean = this.getBit(4)

/**
 * Gets the 6th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Byte.component6(): Boolean = this.getBit(5)

/**
 * Gets the 7th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Byte.component7(): Boolean = this.getBit(6)

/**
 * Gets the 8th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Byte.component8(): Boolean = this.getBit(7)