package boolUtil.fromNumber


/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Turns the given [Short] in to a [BooleanArray] corresponding to the value of its bits.
 *
 * @return [BooleanArray]
 */
fun Short.toBooleanArray(): BooleanArray = booleanArrayOf(
	this.getBit(0), this.getBit(1), this.getBit(2), this.getBit(3), 
	this.getBit(4), this.getBit(5), this.getBit(6), this.getBit(7), 
	this.getBit(8), this.getBit(9), this.getBit(10), this.getBit(11), 
	this.getBit(12), this.getBit(13), this.getBit(14), this.getBit(15)
)

/**
 * Gets the 1st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component1(): Boolean = this.getBit(0)

/**
 * Gets the 2nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component2(): Boolean = this.getBit(1)

/**
 * Gets the 3rd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component3(): Boolean = this.getBit(2)

/**
 * Gets the 4th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component4(): Boolean = this.getBit(3)

/**
 * Gets the 5th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component5(): Boolean = this.getBit(4)

/**
 * Gets the 6th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component6(): Boolean = this.getBit(5)

/**
 * Gets the 7th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component7(): Boolean = this.getBit(6)

/**
 * Gets the 8th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component8(): Boolean = this.getBit(7)

/**
 * Gets the 9th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component9(): Boolean = this.getBit(8)

/**
 * Gets the 10th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component10(): Boolean = this.getBit(9)

/**
 * Gets the 11th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component11(): Boolean = this.getBit(10)

/**
 * Gets the 12th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component12(): Boolean = this.getBit(11)

/**
 * Gets the 13th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component13(): Boolean = this.getBit(12)

/**
 * Gets the 14th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component14(): Boolean = this.getBit(13)

/**
 * Gets the 15th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component15(): Boolean = this.getBit(14)

/**
 * Gets the 16th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Short.component16(): Boolean = this.getBit(15)