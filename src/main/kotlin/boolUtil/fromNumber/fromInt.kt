package boolUtil.fromNumber


/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Turns the given [Int] in to a [BooleanArray] corresponding to the value of its bits.
 *
 * @return [BooleanArray]
 */
fun Int.toBooleanArray(): BooleanArray = booleanArrayOf(
	this.getBit(0), this.getBit(1), this.getBit(2), this.getBit(3), 
	this.getBit(4), this.getBit(5), this.getBit(6), this.getBit(7), 
	this.getBit(8), this.getBit(9), this.getBit(10), this.getBit(11), 
	this.getBit(12), this.getBit(13), this.getBit(14), this.getBit(15), 
	this.getBit(16), this.getBit(17), this.getBit(18), this.getBit(19), 
	this.getBit(20), this.getBit(21), this.getBit(22), this.getBit(23), 
	this.getBit(24), this.getBit(25), this.getBit(26), this.getBit(27), 
	this.getBit(28), this.getBit(29), this.getBit(30), this.getBit(31)
)

/**
 * Gets the 1st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component1(): Boolean = this.getBit(0)

/**
 * Gets the 2nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component2(): Boolean = this.getBit(1)

/**
 * Gets the 3rd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component3(): Boolean = this.getBit(2)

/**
 * Gets the 4th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component4(): Boolean = this.getBit(3)

/**
 * Gets the 5th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component5(): Boolean = this.getBit(4)

/**
 * Gets the 6th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component6(): Boolean = this.getBit(5)

/**
 * Gets the 7th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component7(): Boolean = this.getBit(6)

/**
 * Gets the 8th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component8(): Boolean = this.getBit(7)

/**
 * Gets the 9th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component9(): Boolean = this.getBit(8)

/**
 * Gets the 10th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component10(): Boolean = this.getBit(9)

/**
 * Gets the 11th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component11(): Boolean = this.getBit(10)

/**
 * Gets the 12th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component12(): Boolean = this.getBit(11)

/**
 * Gets the 13th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component13(): Boolean = this.getBit(12)

/**
 * Gets the 14th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component14(): Boolean = this.getBit(13)

/**
 * Gets the 15th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component15(): Boolean = this.getBit(14)

/**
 * Gets the 16th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component16(): Boolean = this.getBit(15)

/**
 * Gets the 17th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component17(): Boolean = this.getBit(16)

/**
 * Gets the 18th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component18(): Boolean = this.getBit(17)

/**
 * Gets the 19th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component19(): Boolean = this.getBit(18)

/**
 * Gets the 20th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component20(): Boolean = this.getBit(19)

/**
 * Gets the 21st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component21(): Boolean = this.getBit(20)

/**
 * Gets the 22nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component22(): Boolean = this.getBit(21)

/**
 * Gets the 23rd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component23(): Boolean = this.getBit(22)

/**
 * Gets the 24th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component24(): Boolean = this.getBit(23)

/**
 * Gets the 25th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component25(): Boolean = this.getBit(24)

/**
 * Gets the 26th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component26(): Boolean = this.getBit(25)

/**
 * Gets the 27th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component27(): Boolean = this.getBit(26)

/**
 * Gets the 28th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component28(): Boolean = this.getBit(27)

/**
 * Gets the 29th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component29(): Boolean = this.getBit(28)

/**
 * Gets the 30th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component30(): Boolean = this.getBit(29)

/**
 * Gets the 31st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component31(): Boolean = this.getBit(30)

/**
 * Gets the 32nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Int.component32(): Boolean = this.getBit(31)