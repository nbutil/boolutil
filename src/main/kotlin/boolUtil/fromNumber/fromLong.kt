package boolUtil.fromNumber


/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Turns the given [Long] in to a [BooleanArray] corresponding to the value of its bits.
 *
 * @return [BooleanArray]
 */
fun Long.toBooleanArray(): BooleanArray = booleanArrayOf(
	this.getBit(0), this.getBit(1), this.getBit(2), this.getBit(3), 
	this.getBit(4), this.getBit(5), this.getBit(6), this.getBit(7), 
	this.getBit(8), this.getBit(9), this.getBit(10), this.getBit(11), 
	this.getBit(12), this.getBit(13), this.getBit(14), this.getBit(15), 
	this.getBit(16), this.getBit(17), this.getBit(18), this.getBit(19), 
	this.getBit(20), this.getBit(21), this.getBit(22), this.getBit(23), 
	this.getBit(24), this.getBit(25), this.getBit(26), this.getBit(27), 
	this.getBit(28), this.getBit(29), this.getBit(30), this.getBit(31), 
	this.getBit(32), this.getBit(33), this.getBit(34), this.getBit(35), 
	this.getBit(36), this.getBit(37), this.getBit(38), this.getBit(39), 
	this.getBit(40), this.getBit(41), this.getBit(42), this.getBit(43), 
	this.getBit(44), this.getBit(45), this.getBit(46), this.getBit(47), 
	this.getBit(48), this.getBit(49), this.getBit(50), this.getBit(51), 
	this.getBit(52), this.getBit(53), this.getBit(54), this.getBit(55), 
	this.getBit(56), this.getBit(57), this.getBit(58), this.getBit(59), 
	this.getBit(60), this.getBit(61), this.getBit(62), this.getBit(63)
)

/**
 * Gets the 1st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component1(): Boolean = this.getBit(0)

/**
 * Gets the 2nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component2(): Boolean = this.getBit(1)

/**
 * Gets the 3rd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component3(): Boolean = this.getBit(2)

/**
 * Gets the 4th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component4(): Boolean = this.getBit(3)

/**
 * Gets the 5th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component5(): Boolean = this.getBit(4)

/**
 * Gets the 6th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component6(): Boolean = this.getBit(5)

/**
 * Gets the 7th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component7(): Boolean = this.getBit(6)

/**
 * Gets the 8th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component8(): Boolean = this.getBit(7)

/**
 * Gets the 9th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component9(): Boolean = this.getBit(8)

/**
 * Gets the 10th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component10(): Boolean = this.getBit(9)

/**
 * Gets the 11th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component11(): Boolean = this.getBit(10)

/**
 * Gets the 12th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component12(): Boolean = this.getBit(11)

/**
 * Gets the 13th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component13(): Boolean = this.getBit(12)

/**
 * Gets the 14th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component14(): Boolean = this.getBit(13)

/**
 * Gets the 15th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component15(): Boolean = this.getBit(14)

/**
 * Gets the 16th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component16(): Boolean = this.getBit(15)

/**
 * Gets the 17th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component17(): Boolean = this.getBit(16)

/**
 * Gets the 18th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component18(): Boolean = this.getBit(17)

/**
 * Gets the 19th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component19(): Boolean = this.getBit(18)

/**
 * Gets the 20th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component20(): Boolean = this.getBit(19)

/**
 * Gets the 21st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component21(): Boolean = this.getBit(20)

/**
 * Gets the 22nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component22(): Boolean = this.getBit(21)

/**
 * Gets the 23rd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component23(): Boolean = this.getBit(22)

/**
 * Gets the 24th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component24(): Boolean = this.getBit(23)

/**
 * Gets the 25th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component25(): Boolean = this.getBit(24)

/**
 * Gets the 26th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component26(): Boolean = this.getBit(25)

/**
 * Gets the 27th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component27(): Boolean = this.getBit(26)

/**
 * Gets the 28th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component28(): Boolean = this.getBit(27)

/**
 * Gets the 29th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component29(): Boolean = this.getBit(28)

/**
 * Gets the 30th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component30(): Boolean = this.getBit(29)

/**
 * Gets the 31st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component31(): Boolean = this.getBit(30)

/**
 * Gets the 32nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component32(): Boolean = this.getBit(31)

/**
 * Gets the 33rd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component33(): Boolean = this.getBit(32)

/**
 * Gets the 34th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component34(): Boolean = this.getBit(33)

/**
 * Gets the 35th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component35(): Boolean = this.getBit(34)

/**
 * Gets the 36th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component36(): Boolean = this.getBit(35)

/**
 * Gets the 37th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component37(): Boolean = this.getBit(36)

/**
 * Gets the 38th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component38(): Boolean = this.getBit(37)

/**
 * Gets the 39th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component39(): Boolean = this.getBit(38)

/**
 * Gets the 40th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component40(): Boolean = this.getBit(39)

/**
 * Gets the 41st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component41(): Boolean = this.getBit(40)

/**
 * Gets the 42nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component42(): Boolean = this.getBit(41)

/**
 * Gets the 43rd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component43(): Boolean = this.getBit(42)

/**
 * Gets the 44th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component44(): Boolean = this.getBit(43)

/**
 * Gets the 45th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component45(): Boolean = this.getBit(44)

/**
 * Gets the 46th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component46(): Boolean = this.getBit(45)

/**
 * Gets the 47th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component47(): Boolean = this.getBit(46)

/**
 * Gets the 48th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component48(): Boolean = this.getBit(47)

/**
 * Gets the 49th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component49(): Boolean = this.getBit(48)

/**
 * Gets the 50th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component50(): Boolean = this.getBit(49)

/**
 * Gets the 51st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component51(): Boolean = this.getBit(50)

/**
 * Gets the 52nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component52(): Boolean = this.getBit(51)

/**
 * Gets the 53rd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component53(): Boolean = this.getBit(52)

/**
 * Gets the 54th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component54(): Boolean = this.getBit(53)

/**
 * Gets the 55th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component55(): Boolean = this.getBit(54)

/**
 * Gets the 56th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component56(): Boolean = this.getBit(55)

/**
 * Gets the 57th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component57(): Boolean = this.getBit(56)

/**
 * Gets the 58th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component58(): Boolean = this.getBit(57)

/**
 * Gets the 59th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component59(): Boolean = this.getBit(58)

/**
 * Gets the 60th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component60(): Boolean = this.getBit(59)

/**
 * Gets the 61st bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component61(): Boolean = this.getBit(60)

/**
 * Gets the 62nd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component62(): Boolean = this.getBit(61)

/**
 * Gets the 63rd bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component63(): Boolean = this.getBit(62)

/**
 * Gets the 64th bit from the given [Byte]
 *
 * @return [Boolean]
 */
operator fun Long.component64(): Boolean = this.getBit(63)