package boolUtil.fromNumber

/**
 * @author: Curtis Woodard <nbness1337@gmail.com>
 *
 * Gets the [index]th bit of the given [Number]
 *
 * @param [index] The index of the bit to get from the [Number]
 *
 * @return [Boolean]
 */

internal fun Number.getBit(index: Int): Boolean = (this.toLong() and (1 shl index).toLong()) != 0L