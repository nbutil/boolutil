package boolUtil.random

import boolUtil.denseList.DenseBoolList
import kotlin.random.Random

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Generates a random [BooleanArray]
 *
 * @param [size] The size of the [BooleanArray] to generate
 *
 * @return [BooleanArray]
 */
fun randomBooleanArray(size: Int): BooleanArray = BooleanArray(size) { Random.nextBoolean() }

/**
 * Generates a random [Array]<[Boolean]>
 *
 * @param [size] The size of the [Array]<[Boolean]>to generate
 *
 * @return [Array]<[Boolean]>
 */
fun randomTypedBooleanArray(size: Int): Array<Boolean> = Array(size) { Random.nextBoolean() }

/**
 * Generates a random [List]<[Boolean]>
 *
 * @param [size] The size of the [List]<[Boolean]>to generate
 *
 * @return [List]<[Boolean]>
 */
fun randomBooleanList(size: Int): List<Boolean> = List(size) { Random.nextBoolean() }

/**
 * Generates a random [DenseBoolList]
 *
 * @param [size] The size of the [DenseBoolList] to generate
 *
 * @return [DenseBoolList]
 */
fun randomDenseBoolList(size: Int): DenseBoolList = DenseBoolList(size) { Random.nextBoolean() }