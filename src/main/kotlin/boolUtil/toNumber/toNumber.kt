package boolUtil.toNumber

/**
 * @author: Curtis Woodard <nbness1337@gmail.com>
 */

/**
 * Returns the [Byte] value of this [Boolean]
 *
 * @return [Byte]
 */
fun Boolean.toByte(): Byte = if (this) 1 else 0

/**
 * Returns the [Short] value of this [Boolean]
 *
 * @return [Short]
 */
fun Boolean.toShort(): Short = if (this) 1 else 0

/**
 * Returns the [Int] value of this [Boolean]
 *
 * @return [Int]
 */
fun Boolean.toInt(): Int = if (this) 1 else 0

/**
 * Returns the [Float] value of this [Boolean]
 *
 * @return [Float]
 */
fun Boolean.toFloat(): Float = if (this) 1.0f else 0.0f

/**
 * Returns the [Long] value of this [Boolean]
 *
 * @return [Long]
 */
fun Boolean.toLong(): Long = if (this) 1L else 0L

/**
 * Returns the [Double] value of this [Boolean]
 *
 * @return [Double]
 */
fun Boolean.toDouble(): Double = if (this) 1.0 else 0.0