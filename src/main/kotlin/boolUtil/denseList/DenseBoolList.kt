package boolUtil.denseList

import boolUtil.denseList.impls.immutable.BooleanItr
import boolUtil.denseList.impls.immutable.BooleanListItr
import boolUtil.denseList.impls.immutable.BooleanSubList

/**
 * @author: Curtis Woodard <nbness1337@gmail.com>
 *
 * The "standard" equivalent of [DenseBoolList] would be [BooleanArray].
 * [DenseBoolList] is a more memory-minded way to store your [Boolean]s, as it takes 1 bit per boolean rather than 1 byte per boolean.
 * @constructor Creates a [size] size [DenseBoolList] full of `false`
 */
open class DenseBoolList(
    size: Int
): List<Boolean> {

    protected var actualSize: Int = size

    protected val maxIndex: Int
        get() = actualSize - 1


    override val size: Int
        get() = actualSize

    /**
     * @param [size] The size of the [DenseBoolList] that is being created
     * @constructor Creates a [size] size [DenseBoolList] initialized by [initializer]
     */
    constructor(size: Int, initializer: (index: Int) -> Boolean): this(size) {
        for (index in 0 until size) {
            val byteIndex = getByteIndexFor(index)
            internalBytes[byteIndex] = internalBytes[byteIndex].setBit(index, initializer(index))
        }

    }

    /**
     * Where all the storage magic happens in [Long]s.
     * [Long] is used instead of [Byte] because every object has the same reference overhead so [Long] gives you the most bang for your buck no matter how you see it.
     * Minimum object size on JVM is 32 bits, so even separate bytes and booleans use at least 4 bytes each.
     * With [DenseBoolList] each [Boolean] uses 1 bit, you gain at least a 31 bit saving for each [Boolean] in this array.
     */
    protected var internalBytes: ByteArray = ByteArray(if (actualSize > 0) ((actualSize-1)/Byte.SIZE_BITS)+1 else 0)

    /**
     * Creates an iterator for this [DenseBoolList]
     */
    override fun iterator(): Iterator<Boolean> =
        BooleanItr(this)

    /**
     * Returns a view of the portion of this list between the specified [fromIndex] (inclusive) and [toIndex] (exclusive).
     * The returned list is backed by this list, so non-structural changes in the returned list are reflected in this list, and vice-versa.
     *
     * Structural changes in the base list make the behavior of the view undefined.
     */
    override fun subList(fromIndex: Int, toIndex: Int): List<Boolean> =
        BooleanSubList(this, fromIndex, toIndex)

    /**
     * Returns a list iterator over the elements in this list (in proper sequence), starting at the specified [index].
     *
     * @param [index] The index to initialize the [ListIterator] at
     *
     * @return [ListIterator]<[Boolean]>
     */
    override fun listIterator(index: Int): ListIterator<Boolean> =
        BooleanListItr(this, index)

    /**
     * Returns a list iterator over the elements in this list (in proper sequence).
     *
     * @return [ListIterator]<[Boolean]>
     */
    override fun listIterator(): ListIterator<Boolean> = BooleanListItr(this, 0)

    /**
     * Gets the index of the first [element] in this [DenseBoolList]
     */
    override fun indexOf(element: Boolean): Int {
        for ((index, value) in this.withIndex()) {
            if (value == element) {
                return index
            }
        }
        return -1
    }

    /**
     * Gets the last index of [element]
     *
     * @param [element] The element to get the last index of
     *
     * @return Int
     */
    override fun lastIndexOf(element: Boolean): Int {
        var lastIndex = -1
        var currentIndex = 0
        for (value in this) {
            if (value == element) {
                lastIndex = currentIndex++
            }
        }
        return lastIndex
    }

    /**
     * Checks if the given [element] is in this [DenseBoolList]
     *
     * @param [element] The value to check is in this [DenseBoolList]
     *
     * @return [Boolean]
     */
    override operator fun contains(element: Boolean): Boolean = this.any { it == element }

    /**
     * Checks if all values in [elements] are contained in this [DenseBoolList]
     *
     * @param [elements] The elements to check.
     *
     * @return [Boolean]
     */
    override fun containsAll(elements: Collection<Boolean>): Boolean {
        for (element in elements) {
            if (element !in this)
                return false
        }
        return true
    }

    /**
     * Checks if this [DenseBoolList] is empty (size 0)
     *
     * @return [Boolean]
     */
    override fun isEmpty(): Boolean =
        actualSize == 0

    /**
     * This returns the [Boolean]s stored in this [DenseBoolList]
     *
     * @return [String]
     */
    override fun toString(): String {
        var outputString = "["
        for (booleanValue in this.withIndex()) {
            outputString += booleanValue.value
            if (booleanValue.index != maxIndex)
                outputString += ", "
        }
        return "$outputString]"
    }

    /**
     * Gets the value of the [index]th bit
     *
     * @param [index] the index of the bit to get from this
     *
     * @return [Boolean]
     */
    override operator fun get(index: Int): Boolean {
        return ((this.internalBytes[index / Byte.SIZE_BITS].toInt() and -1) and (0b1 shl (index % Byte.SIZE_BITS))) != 0
    }

    fun toMutableDenseBoolList(): MutableDenseBoolList = MutableDenseBoolList(size) { this[it] }

    /**
     * Attempts to get the values corresponding to each index in [indices] and returns them.
     *
     * @param [indices] The actualIndices to get the corresponding values for
     *
     * @return [DenseBoolList]
     */
    operator fun get(indices: IntArray): DenseBoolList =
        DenseBoolList(indices.size) { this[indices[it]] }

    init {
        if (size < 0) {
            throw NegativeArraySizeException("$size")
        }
    }
}
