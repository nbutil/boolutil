package boolUtil.denseList.impls.mutable

import boolUtil.denseList.impls.immutable.BooleanListItr

internal class MutableBooleanListItr(override val toIterateOn: MutableList<Boolean>, currentIndex: Int):
    BooleanListItr(toIterateOn, currentIndex), MutableListIterator<Boolean> {
    /**
     * Adds the specified element [element] into the underlying collection immediately before the element that would be
     * returned by [next], if any, and after the element that would be returned by [previous], if any.
     * (If the collection contains no elements, the new element becomes the sole element in the collection.)
     * The new element is inserted before the implicit cursor: a subsequent call to [next] would be unaffected,
     * and a subsequent call to [previous] would return the new element. (This call increases by one the value \
     * that would be returned by a call to [nextIndex] or [previousIndex].)
     */
    override fun add(element: Boolean) {
        toIterateOn.add(lastIndex, element)
    }

    /**
     * Replaces the last element returned by [next] or [previous] with the specified element [element].
     */
    override fun set(element: Boolean) {
        toIterateOn[lastIndex] = element
    }

    override fun remove() {
        toIterateOn.removeAt(lastIndex)
    }
}