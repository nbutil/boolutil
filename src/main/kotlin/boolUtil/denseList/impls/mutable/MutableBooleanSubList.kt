package boolUtil.denseList.impls.mutable

import boolUtil.denseList.MutableDenseBoolList
import boolUtil.denseList.impls.immutable.BooleanSubList

internal class MutableBooleanSubList(
    override val backingList: MutableList<Boolean>,
    fromIndex: Int, toIndex: Int
) : BooleanSubList(backingList, fromIndex, toIndex), MutableList<Boolean> {
    override fun iterator(): MutableIterator<Boolean> = MutableBoolItr(this)

    protected fun increaseSizeBy(amount: Int) { toIndex += amount }
    protected fun decreaseSizeBy(amount: Int) { toIndex -= amount }

    /**
     * Adds the specified element to the end of this list.
     *
     * @return `true` because the list is always modified as the result of this operation.
     */
    override fun add(element: Boolean): Boolean {
        increaseSizeBy(1)
        backingList.add(actualIndexFromLocal(localIndices.last), element)
        return true
    }

    /**
     * Inserts an element into the list at the specified [index].
     */
    override fun add(index: Int, element: Boolean) {
        increaseSizeBy(1)
        backingList.add(actualIndexFromLocal(index), element)
    }

    /**
     * Inserts all of the elements of the specified collection [elements] into this list at the specified [index].
     *
     * @return `true` if the list was changed as the result of the operation.
     */
    override fun addAll(index: Int, elements: Collection<Boolean>): Boolean {
        if (elements.isEmpty()) return false
        var actualIndex = index
        for (element in elements) {
            increaseSizeBy(1)
            add(actualIndex++, element)
        }
        return true
    }

    /**
     * Adds all of the elements of the specified collection to the end of this list.
     *
     * The elements are appended in the order they appear in the [elements] collection.
     *
     * @return `true` if the list was changed as the result of the operation.
     */
    override fun addAll(elements: Collection<Boolean>): Boolean {
        if (elements.isEmpty()) return false
        for(element in elements) {
            add(element)
        }
        return true
    }

    override fun clear() {
        while (this.isNotEmpty()) {
            backingList.removeAt(actualIndexFromLocal(0))
            decreaseSizeBy(1)
        }
    }

    override fun listIterator(): MutableListIterator<Boolean> = MutableBooleanListItr(this, 0)

    override fun listIterator(index: Int): MutableListIterator<Boolean> = MutableBooleanListItr(this, index)

    override fun remove(element: Boolean): Boolean {
        for ((localIndex, thing) in this.withIndex()) {
            if (thing == element) {
                backingList.removeAt(actualIndexFromLocal(localIndex))
                decreaseSizeBy(1)
                return true
            }
        }
        return false
    }

    fun removeAllOf(element: Boolean): Boolean {
        var didRemoveValue = false
        var currentIndex = 0
        var indexOfLast = this.indexOfLast { element == it }
        while (true) {
            val currentValue = this[currentIndex]
            if (currentValue == element) {
                removeAt(currentIndex)
                if (currentIndex == indexOfLast) {
                    break
                }
                indexOfLast--
                didRemoveValue = true
            } else {
                currentIndex++
            }
        }
        return didRemoveValue
    }

    override fun removeAll(elements: Collection<Boolean>): Boolean {
        var wasModified = false
        for (element in elements) {
            if (remove(element)) {
                wasModified = true
            }
        }
        return wasModified
    }

    /**
     * Removes an element at the specified [index] from the list.
     *
     * @return the element that has been removed.
     */
    override fun removeAt(index: Int): Boolean {
        backingList.removeAt(actualIndexFromLocal(index))
        decreaseSizeBy(1)
        return true
    }

    override fun retainAll(elements: Collection<Boolean>): Boolean {
        if (elements.isEmpty()) return false
        var wasModified = false
        for (element in elements) {
            if (element !in this) {
                if (removeAllOf(element)) {
                    wasModified = true
                }
            }
        }
        return wasModified
    }

    /**
     * Replaces the element at the specified position in this list with the specified element.
     *
     * @return the element previously at the specified position.
     */
    override fun set(index: Int, element: Boolean): Boolean {
        if (this[index] == element) return false
        backingList[actualIndexFromLocal(index)] = element
        return true
    }

    override fun subList(fromIndex: Int, toIndex: Int): MutableList<Boolean> = MutableBooleanSubList(this, fromIndex, toIndex)

}
