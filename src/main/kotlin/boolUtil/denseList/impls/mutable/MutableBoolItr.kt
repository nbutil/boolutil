package boolUtil.denseList.impls.mutable

import boolUtil.denseList.impls.immutable.BooleanItr

internal class MutableBoolItr(override val toIterateOn: MutableList<Boolean>): BooleanItr(toIterateOn), MutableIterator<Boolean> {
    override fun remove() {
        toIterateOn.removeAt(lastIndex)
    }
}