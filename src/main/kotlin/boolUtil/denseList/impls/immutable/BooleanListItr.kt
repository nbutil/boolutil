package boolUtil.denseList.impls.immutable

/**
 * @author: Curtis Woodard <nbness1337@gmail.com>
 *
 * Guessing this is just an [Iterator] that can go backwards as well? Hope so haha :grimace:
 *
 * @property [toIterateOn] The [List]<[Boolean]> to iterate over
 * @property [currentIndex] The index that the [BooleanListItr] will start on
 *
 * @constructor Creates a [BooleanListItr] to iterate over [toIterateOn] starting at index [currentIndex]
 */
internal open class BooleanListItr(toIterateOn: List<Boolean>, override var currentIndex: Int): BooleanItr(toIterateOn), ListIterator<Boolean> {
    override fun nextIndex(): Int = if (hasNext()) currentIndex + 1 else 0
    override fun previousIndex(): Int = if (hasPrevious()) currentIndex - 1 else toIterateOn.size - 1
    override fun previous(): Boolean {
        if (currentIndex < 0 || currentIndex >= toIterateOn.size) {
            currentIndex = toIterateOn.size - 1
        }
        lastIndex = currentIndex
        return toIterateOn[currentIndex--]
    }
    override fun hasPrevious(): Boolean = currentIndex >= 0
}