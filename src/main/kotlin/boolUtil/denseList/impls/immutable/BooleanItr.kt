package boolUtil.denseList.impls.immutable

/**
 * @author: Curtis Woodard <nbness1337@gmail.com>
 *
 * I have no idea how to implement any kind of [Iterator] so here goes nothing...
 *
 * @property [toIterateOn] the [List]<[Boolean]> to iterate over
 *
 * @constructor Creates a [BooleanItr] to iterate over
 */
internal open class BooleanItr(internal open val toIterateOn: List<Boolean>): BooleanIterator() {
    internal open var currentIndex: Int = 0
    protected var lastIndex: Int = -1

    /**
     * Gives the next [Boolean] in relation to the for loop that the [List]<[Boolean]> is in
     */
    override fun nextBoolean(): Boolean {
        if (currentIndex >= toIterateOn.size) {
            currentIndex = 0
        }
        lastIndex = currentIndex
        return toIterateOn[currentIndex++]
    }

    /**
     * Checks if there is another [Boolean] "after" the last one.
     *
     * @return [Boolean]
     */
    override fun hasNext(): Boolean = currentIndex < toIterateOn.size
}