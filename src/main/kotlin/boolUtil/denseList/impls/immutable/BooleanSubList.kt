package boolUtil.denseList.impls.immutable

internal open class BooleanSubList(open val backingList: List<Boolean>, var fromIndex: Int, var toIndex: Int): List<Boolean> {

    override val size
        get() = toIndex - fromIndex

    val actualIndices
        get() = fromIndex until toIndex

    val localIndices
        get() = 0 until size

    private val zippedIndices
        get() = actualIndices zip localIndices

    protected fun actualIndexFromLocal(index: Int): Int {
        for ((actualIndex, localIndex) in zippedIndices) {
            if (index == localIndex) {
                return actualIndex
            }
        }
        throw ArrayIndexOutOfBoundsException("Invalid index in sublist: $index")
    }

    override fun contains(element: Boolean): Boolean {
        for (bool in this) {
            if (bool == element) {
                return true
            }
        }
        return false
    }

    override fun containsAll(elements: Collection<Boolean>): Boolean {
        for (element in elements) {
            if (!element in this) {
                return false
            }
        }
        return true
    }

    override fun get(index: Int): Boolean = backingList[actualIndexFromLocal(index)]

    override fun indexOf(element: Boolean): Int {
        for ((actualIndex, localIndex) in zippedIndices) {
            if (backingList[actualIndex] == element) {
                return localIndex
            }
        }
        return -1
    }

    override fun isEmpty(): Boolean = this.size == 0

    override fun iterator(): Iterator<Boolean> = BooleanItr(this)

    override fun lastIndexOf(element: Boolean): Int {
        var lastIndex: Int = -1
        for ((index, bool) in this.withIndex()) {
            if (bool == element) {
                lastIndex = index
            }
        }
        return lastIndex
    }

    override fun listIterator(): ListIterator<Boolean> = BooleanListItr(this, 0)

    override fun listIterator(index: Int): ListIterator<Boolean> =
        BooleanListItr(this, index)

    override fun subList(fromIndex: Int, toIndex: Int): List<Boolean> =
        BooleanSubList(this, fromIndex, toIndex)

    override fun toString(): String = this.joinToString(prefix = "[", postfix = "]")
}
