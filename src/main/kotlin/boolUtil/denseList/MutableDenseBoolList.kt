package boolUtil.denseList

import boolUtil.denseList.impls.mutable.MutableBoolItr
import boolUtil.denseList.impls.mutable.MutableBooleanListItr
import boolUtil.denseList.impls.mutable.MutableBooleanSubList

class MutableDenseBoolList(
    size: Int
): DenseBoolList(size), MutableList<Boolean> {

    constructor(size: Int, initializer: (index: Int) -> Boolean): this(size) {
        for (index in 0 until size) {
            this[index] = initializer(index)
        }
    }

    private fun decreaseBytesToBitSize(newBitSize: Int) {
        if (newBitSize >= actualSize) return
        if ((newBitSize - 1) % Byte.SIZE_BITS != 0) return
        val newByteSize = (newBitSize / Byte.SIZE_BITS) + 1
        if (newByteSize < internalBytes.size) {
            internalBytes = ByteArray(newByteSize) { if (it in internalBytes.indices) internalBytes[it] else 0 }
        }
    }

    private fun expandBytesToBitSize(newBitSize: Int) {
        if (newBitSize <= actualSize) return
        if ((newBitSize - 1) % Byte.SIZE_BITS != 0) return
        val newByteSize = (newBitSize / Byte.SIZE_BITS) + 1
        if (newByteSize > internalBytes.size) {
            internalBytes = ByteArray(newByteSize) { if (it in internalBytes.indices) internalBytes[it] else 0 }
        }
    }

    override fun iterator(): MutableIterator<Boolean> =
        MutableBoolItr(this)

    /**
     * Adds the specified element to the end of this list.
     *
     * @return `true` because the list is always modified as the result of this operation.
     */
    override fun add(element: Boolean): Boolean {
        expandBytesToBitSize(actualSize + 1)
        this[actualSize++] = element
        return true
    }

    /**
     * Inserts an element into the list at the specified [index].
     *
     * Different than the regular [List] [add], lets you put something in the "new last index" if you are adding on to the end
     */
    override fun add(index: Int, element: Boolean) {
        expandBytesToBitSize(actualSize + 1)
        // setting right to left because left to right when expanding to the right corrupts
        for (setIndex in maxIndex downTo index) {
            this[setIndex] = this[setIndex - 1]
        }
        actualSize++
        this[index] = element
    }

    /**
     * Adds all of the elements of the specified collection to the end of this list.
     *
     * The elements are appended in the order they appear in the [elements] collection.
     *
     * @return `true` if the list was changed as the result of the operation.
     */
    override fun addAll(elements: Collection<Boolean>): Boolean {
        if (elements.isEmpty()) return false
        for (element in elements) add(element)
        return true

    }

    /**
     * Inserts all of the elements of the specified collection [elements] into this list at the specified [index].
     *
     * @return `true` if the list was changed as the result of the operation.
     */
    override fun addAll(index: Int, elements: Collection<Boolean>): Boolean {
        if (elements.isEmpty()) return false
        var actualIndex = index
        for (element in elements) add(actualIndex++, element)
        return true
    }

    override fun clear() {
        actualSize = 0
        this.internalBytes = byteArrayOf()
    }

    override fun listIterator(): MutableListIterator<Boolean> = MutableBooleanListItr(this, 0)

    override fun listIterator(index: Int): MutableListIterator<Boolean> = MutableBooleanListItr(this, index)

    override fun remove(element: Boolean): Boolean {
        for (index in indices) {
            val currentElement = this[index]
            if (element == currentElement) {
                removeAt(index)
                return true
            }
        }
        return false
    }

    fun removeAllOf(element: Boolean): Boolean {
        var didRemoveValue = false
        var currentIndex = 0
        var indexOfLast = this.indexOfLast { element == it }
        while (true) {
            val currentValue = this[currentIndex]
            if (currentValue == element) {
                removeAt(currentIndex)
                if (currentIndex == indexOfLast) {
                    break
                }
                indexOfLast--
                didRemoveValue = true
            } else {
                currentIndex++
            }
        }
        return didRemoveValue
    }

    override fun removeAll(elements: Collection<Boolean>): Boolean {
        var didRemoveValue = false
        for (element in elements) {
            if (remove(element)) {
                didRemoveValue = true
            }
        }
        return didRemoveValue
    }

    /**
     * Removes an element at the specified [index] from the list.
     *
     * @return the element that has been removed.
     */
    override fun removeAt(index: Int): Boolean {
        val removed = this[index]
        for (currentIndex in index until maxIndex) {
            this[currentIndex] = this[currentIndex + 1]
        }
        decreaseBytesToBitSize(actualSize - 1)
        actualSize--
        return removed
    }

    override fun retainAll(elements: Collection<Boolean>): Boolean {
        var didRemoveValue = false
        for (item in this) {
            if (item !in elements) {
                removeAllOf(item)
                didRemoveValue = true
            }
        }
        return didRemoveValue
    }

    /**
     * Replaces the element at the specified position in this list with the specified element.
     *
     * @return the element previously at the specified position.
     */
    override fun set(index: Int, element: Boolean): Boolean {
        if (index !in indices) {
            throw IndexOutOfBoundsException("index $index out of bounds $indices")
        }
        val (byteIndex, bitIndex) = getByteAndBitIndexFor(index)
        val returnValue = this[index]
        val byteToChange = this.internalBytes[byteIndex]
        this.internalBytes[byteIndex] = byteToChange.setBit(bitIndex, element)
        return returnValue
    }

    override fun subList(fromIndex: Int, toIndex: Int): MutableList<Boolean> =
        MutableBooleanSubList(this, fromIndex, toIndex)

    fun contentEquals(other: List<Boolean>): Boolean {
        this.forEachIndexed {index, item -> if (!item == other[index]) return false }
        return true
    }

    fun toDenseBoolList(): DenseBoolList =
        DenseBoolList(size) { this[it] }
}