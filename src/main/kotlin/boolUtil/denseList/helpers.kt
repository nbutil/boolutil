package boolUtil.denseList

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * A helper function to make [DenseBoolList]
 *
 * @return [DenseBoolList]
 */
fun denseBoolListOf(vararg booleans: Boolean) =
    DenseBoolList(booleans.size) { booleans[it] }

/**
 * A helper function to make [MutableDenseBoolList]
 *
 * @return [MutableDenseBoolList]
 */
fun mutableDenseBoolListOf(vararg booleans: Boolean) =
    MutableDenseBoolList(booleans.size) { booleans[it] }

/**
 * Converts the [BooleanArray] to a [DenseBoolList]
 *
 * @return [DenseBoolList]
 */
fun BooleanArray.toDenseBoolList(): DenseBoolList =
    DenseBoolList(size) { this[it] }

/**
 * Converts the given [Array]<[Boolean]> to a [DenseBoolList]
 *
 * @return [DenseBoolList]
 */
fun Array<Boolean>.toDenseBoolList(): DenseBoolList =
    DenseBoolList(size) { this[it] }

/**
 * Converts the given [List]<[Boolean]> to a [DenseBoolList]
 *
 * @return [DenseBoolList]
 */
fun List<Boolean>.toDenseBoolList(): DenseBoolList =
    DenseBoolList(size) { this[it] }

internal fun Byte.setBit(bitIndex: Int, newValue: Boolean): Byte {
    val valueToChange = this.toInt()
    return if (newValue) {
        (valueToChange or (1 shl (bitIndex % Byte.SIZE_BITS)) and -1)
    } else {
        (valueToChange and (1 shl (bitIndex % Byte.SIZE_BITS)).inv() and -1)
    }.toByte()
}

internal fun getByteIndexFor(bitIndex: Int): Int =
    bitIndex / Byte.SIZE_BITS

internal fun getByteBitIndexFor(bitIndex: Int): Int =
    bitIndex % Byte.SIZE_BITS

internal fun getByteAndBitIndexFor(bitIndex: Int): Pair<Int, Int> =
    (bitIndex / Byte.SIZE_BITS) to (bitIndex % Byte.SIZE_BITS)

fun main() {
    println(getByteAndBitIndexFor(0))
    println(getByteAndBitIndexFor(7))
    println(getByteAndBitIndexFor(8))
}